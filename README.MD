# SCIKIT-Learn POC for Batch processing

This project is a sandbox for using Scikit-learn framework with Performance Guardian.

## Install

Is required to have Python 3 and Jupyter Notebooks for a better experience with Machine Learning and data exploration, but its not required.
Using Python virtual environments is strong sugested: https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/

Run:

```bash
    python3 -m virtualenv env
    source env/bin/activate
    pip install -r requirements.txt
    python etl.py MOCK_DATA.csv
    python model.py
```

## ETL
Connects to a configured CassandraDB decribed in  program, pulls the records of the table `pg_traces.pg_system_traces` and then the rows are collapsed using host and date as the key for grouping measurements. The result should look like this:

```csv
,hostname,datetime,MEMORY_UTILIZATION_PERCENT,class
0,PetClinic,2021-03-06 12:14:00,20,OK
1,PetClinic,2021-03-06 12:15:00,21,OK
2,PetClinic,2021-03-06 12:16:00,22,OK
```
Notice the MOCK_DATA.csv contains an example of the expected structure an values that the cassandra table could have. 
Input for the etl program should look like this:

```csv
,hostname,metric,id,measure,timestamp,unit,value
1,host1,CPU,cb2fcb42-e307-4df0-8da1-9a7508653676,UTILIZATION,1614015387000,PERCENT,34.68495
2,host1,MEMORY,2e12fd1a-e9b6-4189-ab17-7a767db7f3a4,UTILIZATION,1614020619000,PERCENT,47.61632
3,host1,MEMORY,52ca9e39-7dc1-4945-953e-026c7157e064,UTILIZATION,1614025215000,PERCENT,73.59597
```


## Model Generation

Once the data is already read and transformed by the `etl.py`, it can be used as input for the machine learning algorithm program `model.py`. Here the data will be formatted again for the model. In this example the features for the machine learning algorithm will be the percentage usage per minute for a defined period `WINDOW_SIZE` very similar to a rolling window. For example a defined `WINDOW_SIZE=5` will create records containing the percentage values from the previous 5 minutes inclusive the time of measurement:

![RollingWindow](img/window1.png)

Once the model is executed it will display the prediction graphic:


![Figure_1](img/Figure_1.png)