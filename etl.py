#!/usr/bin/python

import sys, getopt
import os.path
import numpy as np
import pandas as pd
from datetime import datetime
from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider



#helper function to transform results to pandas
def pandas_factory(colnames, rows):
    return pd.DataFrame(rows, columns=colnames)

default_file_name = "data.csv"

#DS Specfic constants
METRIC = 'metric'
MEASURE = 'measure'
UNIT = 'unit'
METRIC_TUPLE = [METRIC, MEASURE, UNIT]
DATETIME='datetime'
TIMESTAMP='timestamp'
DATE='date'
TIME='time'
HOSTNAME='hostname'

#Column separator
SEPARATOR='_'

def get_data(file,reload=False):
    if(not os.path.isfile(file) or reload):
        print("Fetching data from DB...")
        #fetch data from Cass
        auth_provider = PlainTextAuthProvider(username="admin", password="Iwngtptn1e")
        cluster = Cluster(contact_points=["localhost"], port=9042,
            auth_provider=auth_provider)

        session = cluster.connect("pg_traces")
        session.row_factory = pandas_factory
        session.default_fetch_size = None
        query = "select * from pg_system_traces;"
        rslt = session.execute(query,timeout=120)
        df = rslt._current_rows
        #df.to_csv(file)
        return df
    else:
        print("Reading file content...")
        df = pd.read_csv(file)
        return df

def col_name(colNames: tuple):
    return SEPARATOR.join(map(str,colNames))

def copy_value(record):
    col = col_name([record.metric,record.measure,record.unit])
    record[col] = record.value
    return record

def process(df):
    # Transforming metrics into values
    # 0) remove invalid records
    df = df[df.value.notnull()]
    # 1) checking new column names
    generated_columns = list()
    for group in df.groupby(METRIC_TUPLE).groups:
        new_group_name = col_name(group)
        generated_columns.append(new_group_name)
    # 2) create new columns empty
    for group in generated_columns:
        df[group] = np.NaN#pd.Series([], dtype='float')
    # 3) copy value to corresponding column column
    df = df.transform(copy_value,'columns')
    # 4) Group records per host and hour,minute of reading
    # map per datetime minutes
    df[DATETIME] = df[TIMESTAMP].apply(lambda x: datetime.fromtimestamp(x/1e3).replace(second=0,microsecond=0))
    # This key can be avoided using join on multiple columns, but didnt work rightway with custom aggregator function.
    MAPKEY='mapkey'
    df[MAPKEY] = df[HOSTNAME].str.cat(df[DATETIME].apply(lambda x: str(x)),sep=SEPARATOR)
    #reduce values
    def reduce_mean(group, generated_columns):
        record = group.iloc[0].copy()
        for col in generated_columns:
            val = group[col].apply(pd.to_numeric,errors='coerce').mean()
            record[col] = val
        return record
    df = df.groupby(MAPKEY,group_keys=False).aggregate(reduce_mean,generated_columns).reset_index()

    # 5) Write to csv file formatted
    output_columns = [HOSTNAME,DATETIME] + generated_columns
    return df[output_columns]

def label_by_datetime(row_datetime,date):
    if row_datetime < date:
        return 'ERROR'
    return 'OK'

def main():
    file = default_file_name
    if(len(sys.argv) > 1 and sys.argv[1] is not None):
        file = sys.argv[1]
    df = get_data(file)
    df = process(df)

    #Manual classification.  
    normal_day = datetime.fromisoformat('2021-03-07T00:00:00')
    df['class'] = df[DATETIME].apply(lambda x: label_by_datetime(x,normal_day))
    df.to_csv("etl.csv")


if __name__ == "__main__":
    main()